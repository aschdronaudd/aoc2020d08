#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <vector>
#include <utility>
#include <optional>

using OpType = std::vector<std::pair<uint, int>>;

enum Instructions { NOP, ACC, JMP };
std::unordered_map<std::string, uint> Op { {"nop", 0}, {"acc", 1}, {"jmp", 2} };

std::string num2op(uint num){
	switch(num){
		case 0:
			return "nop";
		case 1:
			return "acc";
		case 2:
			return "jmp";
		default:
			throw("Tantrum");
	}
}

std::ostream& operator<<(std::ostream &os, const std::unordered_map<std::string, uint> um){
	for(auto& el: um){
		os << el.first << "->" << el.second << '\n';
	}
	return os;
}
std::ostream& operator<<(std::ostream &os, const OpType &vp){
	for(auto& el: vp){
		os << num2op(el.first) << "->" << el.second << '\n';
	}
	return os;
}

size_t step(const OpType &ops, size_t idx, int &acc) {
	switch(ops[idx].first){
		case 0:
			return 1;
		case 1:
			acc += ops[idx].second;
			return 1;
		case 2:
			return ops[idx].second;
	}
}

std::optional<int> loop_test(const OpType &ops){
	size_t idx = 0;
	int acc = 0;
	std::vector<bool> visited(ops.size(), 0);
	while(idx < ops.size()){
		if(visited[idx])
			return std::nullopt;
		visited[idx] = true;
		idx += step(ops, idx, acc);
	}
	if(idx > ops.size())
		throw("instructions went off the rails");
	return acc;
}

int repair(OpType ops){
	for(auto &el: ops){
		std::optional<int> total;
		switch(el.first){
			case 0:
				el.first = JMP;
				total = loop_test(ops);
				if(total){
					return *total; 
				}else
					el.first = NOP;
				break;
			case 2:
				el.first = NOP;
				total = loop_test(ops);
				if(total){
					return *total; 
				}else
					el.first = JMP;
				break;
			default:
				break;
		}
	}
	throw("found nothing");
	return 0;
}

OpType getInput(const char * infile){
	OpType ops;
	std::ifstream input(infile);
	std::string line;
	while( getline(input, line) ){
		std::stringstream ss(line);
		std::string instruction;
		int arg;
		ss >> instruction >> arg;
		ops.emplace_back(std::make_pair(Op[ instruction.c_str() ], arg));
	}
	return ops;
}

int main(int argc, char *argv[])
{
	OpType ops( getInput(argv[1]) );

	//part1
	size_t idx = 0;
	int acc = 0;
	std::vector<bool> visited(ops.size(), 0);
	while(true){
		if(visited[idx])
			break;
		visited[idx] = true;
		idx += step(ops, idx, acc);
	}
	std::cout << acc << '\n';

	//part2
	acc = repair(ops);
	std::cout << acc << '\n';
	
	return 0;
}
